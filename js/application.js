visuaLOD.controller('LDController', function($scope, $http, $compile, $timeout, $location, ld) {

  $scope.ld = {}; // linked data javascript object
  $scope.context = {};

  var viewsData = [], // tmp
      viewsPath = $scope.viewsPath; // defined with $rootScope.viewsPath
      views = $scope.views,
      appRoot = angular.isDefined($scope.appRoot) ? $scope.appRoot : '';

  $scope.viewsData = [];
  $scope.fetch = undefined;
  $scope.loading = false;
  $scope.urierror = false;
  $scope.warning = false;
  $scope.urlChanged = false;

  /**
   * Init function. URL validation. Resets ld object and all views if startURI changes.
   * Gets start resource and puts the result in the $scope.ld object.
   * @see  $watch('ld') to see what happens next with this object
   */
  $scope.init = function() {
    $scope.loading = true;
    var uri = ((/^https?:\/\//.test($scope.startURI))) ? $scope.startURI : 'http://'+$scope.startURI;
    if(!$scope.isURL(uri)) { return; }

    $scope.reset(function(success) {
      if(!success) { return; }
      // $scope.ld = ld.getResource(uri, $scope.context);

      ld.getResource(uri, $scope.context).then(function(data) {
        delete data['@context'];
        var alternate = data['http://www.w3.org/1999/xhtml/vocab#alternate'],
            primaryTopic = data['foaf:primaryTopic'] || data['foaf:primarytopic'] || alternate;
        if(
          (angular.isUndefined(data['@type']) || data['@type'] === 'foaf:Document') &&
          angular.isDefined(primaryTopic)
          ) {
          var correctURI = primaryTopic['@id'];
          $scope.startURI = correctURI;
          $scope.urlChanged = true;
          $scope.init();
        } else {
          $scope.ld = data;
          $scope.loading = false;
        }
      }, function(error) {
        if(angular.isDefined(error)) { console.log(error); }
        $scope.warning = true;
        $scope.loading = false;
      });
    });
  };

  /**
   * Resets ld object and all views. Uses the tmp viewsData array to get
   * all views. Merges the default context and all contextes from views in one.
   * Also gets the defined fetch if available in one view.
   * @see http://stackoverflow.com/questions/929776/merging-associative-arrays-javascript
   * @param  {Function} callback returns true if loop is finished, otherwise false.
   */
  $scope.reset = function(callback) {
    angular.forEach(Object.keys($scope.ld), function(key) {
      delete $scope[key];
    });
    $scope.ld = {};
    $scope.count = 0;
    viewsData = [];
    $scope.viewsData = [];

    angular.forEach(views, function(view, index) {
      var view_url = viewsPath+view+'.json';

      $http.get(view_url).success(function(data) {
        if(!$scope.checkURIConstraint(data.constraints)) {
          callback((index === views.length-1) ? true : false);
          return;
        }
        viewsData.push(data);
        angular.extend($scope.context, data.context);
        if (angular.isDefined(data.fetch) && angular.isUndefined($scope.fetch)) {
          $scope.fetch = data.fetch;
        }
        callback((index === views.length-1) ? true : false);
      });
    });
  };

  $scope.$watch('views.length', function() {
    $scope.init();
  });

  /**
   * When $scope.ld is not empty it will extend $scope with this object,
   * so in the template there is no need to use {{ld.name}}, instead you can use only {{name}}.
   * Also calls the renderViews and nestResource function if there is a new ld object.
   */
  $scope.$watch('ld', function() {
    if(Object.keys($scope.ld).length === 0) { return; }
    $scope.ld = angular.isDefined($scope.ld['$$v']) ? $scope.ld['$$v'] : $scope.ld;

    if(
      $scope.ld === 'null' ||
      ($scope.count === 1 && angular.isUndefined($scope.ld['@id']))
    ) {
      $scope.warning = true;
      $scope.loading = false;
      return;
    }

    console.log($scope.ld);

    if($scope.count === 0) {
      $scope.renderViews();
      $scope.nestResource();
    }

    angular.extend($scope, $scope.ld);
    $scope.count++;
  }, true);

  /**
   * Gets all necessary resources from the query defined.
   * Query has to be a uri in the $scope.ld object
   * (therefore needs to be defined in the context).
   */
  $scope.nestResource = function() {
    angular.forEach($scope.fetch, function(query) {
      if(query.indexOf('/') !== -1) { // query goes further

        var path = query.split(' / ');
        // $scope.resolveUri($scope.ld[path[0]]);
        // $scope.resolveUri($scope.ld[path[0]][path[1]]);
        var first_uri = $scope.ld[path[0]];

        $scope.resolveUri(first_uri, function(success) {
          if(success) {
            if(angular.isObject(success)) {
              var data = angular.isDefined(success['@graph']) ? success['@graph'] : success;
              $scope.ld[path[0]] = data;
            }

            angular.forEach($scope.ld[path[0]], function(obj) {
              var second_uri = obj[path[1]];

              if(angular.isDefined(second_uri)) {
                if(angular.isDefined(second_uri[0]) && second_uri[0].indexOf('.jpg') !== -1) {
                  $scope.ld[path[0]][path[1]] = second_uri;
                } else {
                  $scope.resolveUri(second_uri);
                }
              }
            });
          }
        });

      } else {
        $scope.resolveUri($scope.ld[query], function(data) {
          if(angular.isDefined(data['@id']) || angular.isDefined(data['@graph'])) {
            $scope.ld[query] = data;
          }
        });
      }
    });
  };

  /**
   * Replaces uris with json data. resource_s can either be an String or an Array.
   * The splice method removes the uri from the array and adds the loaded data
   * as object on this position (index).
   * @param {String|Array} resource_s uris in $scope.ld object
   * @param {Function} callback (if undefined use angular placeholder function)
   */
  $scope.resolveUri = function(resource_s, callback) {
    $scope.loading = true;
    callback = angular.isUndefined(callback) ? angular.noop : callback;

    if(angular.isArray(resource_s)) {
      angular.forEach(resource_s, function(resource, index) {

        ld.getResource(resource, $scope.context).then(function(data) {
          if (data !== 'null') {
            resource_s.splice(index, 1, data); // remove uri from array, and add object
          }
          $scope.loading = false;
          callback((index === resource_s.length-1) ? true : false);
        }, function(error) {
          if(angular.isDefined(error)) { console.log(error); }
          $scope.loading = false;
        });

      });
    } else {
      ld.getResource(resource_s, $scope.context).then(function(data) {
        callback(data);
        $scope.loading = false;
      }, function(error) {
        if(angular.isDefined(error)) { console.log(error); }
        $scope.loading = false;
      });
    }

  };

  $scope.renderViews = function() {
    $scope.viewsData = [];
    angular.forEach(viewsData, function(view, index) {

      if($scope.checkConstraints(view.constraints)) {
        var newView = true;
        angular.forEach($scope.viewsData, function(existingView) {
          if (existingView.id === view.id) {
            newView = false;
          }
        });
        if(newView) {
          $scope.viewsData.push(view);
        }
        $scope.compileView(view);
      } else {
        console.log('constraints are not true for view: '+view.id);
      }

    });
  };

  /**
   * Checks type and required properties. Minimum 1 type match.
   * All required properties needs to be defined in the ld object.
   * @param  {Object} constraints with type and required
   * @return {Boolean}
   */
  $scope.checkConstraints = function(constraints) {
    if(angular.isUndefined(constraints)) {
      return true;
    }
    var type = constraints.type,
        required = constraints.required,
        checkRequired = true,
        checkType = angular.isUndefined(type); // if type is not defined constraint is true

    type = angular.isArray(type) ? type : [type];

    if (angular.isDefined(required)) {
      required = angular.isArray(required) ? required : [required];
      angular.forEach(required, function(value) {
        if (checkRequired && angular.isUndefined($scope.ld[value])) {
          checkRequired = false;
        }
      });
    }

    angular.forEach(type, function(value) {
      if (checkType || (angular.isDefined($scope.ld['@type']) &&
        (value === $scope.ld['@type'] ||
        $scope.ld['@type'].indexOf(value) !== -1))) {
        checkType = true;
      }
    });

    if(checkType && checkRequired) {
      return true;
    }
    return false;
  };

  /**
   * Checks uri properties. If uri defined it has to match
   * with the startURI.
   * @param  {Object} constraints with optional uris
   * @return {Boolean}
   */
  $scope.checkURIConstraint = function(constraints) {
    if(angular.isUndefined(constraints) || angular.isUndefined(constraints.uri)) {
      return true;
    }
    var uri_constraint = angular.isArray(constraints.uri) ? constraints.uri : [constraints.uri],
        checkuri = false;

    angular.forEach(uri_constraint, function(uri) {
      if($scope.startURI.match(uri)) {
        checkuri = true;
      }
    });
    return checkuri;
  };

  /**
   * Gets template and if necessary also the nested template.
   * $compile tells Angular to recompile the contents.
   * @param {Object} view
   * @param {String} template html
   */
  $scope.compileView = function(view, template) {
    if(angular.isUndefined(template)) {
      $scope.getTemplate(view, function(data) {
        $scope.compileView(view, data);
      });
      return;
    }

    var target = angular.element('#'+view.id),
        nestedTemplate = template.match(/{{@\w+}}/);

    if(nestedTemplate) {
      var result = nestedTemplate[0],
          nestedView = result.replace('{{@','').replace('}}',''),
          view_url = viewsPath+nestedView+'.json';

      $http.get(view_url).success(function(nestedView) {

        if($scope.checkConstraints(nestedView.constraints)) {
          $scope.getTemplate(nestedView, function(data) {
            template = template.replace(result, data);
            $scope.compileView(view, template);
            return;
          });
          return;
        } else {
          template = template.replace(result, '');
          $scope.compileView(view, template);
          return;
        }
      });

    } else {
      if(target.length) {
        target.html($compile(template)($scope));
      } else {
        $timeout(function() {
          $scope.compileView(view, template);
        }, 1500);
      }
    }
    $scope.showFirstTab();

  };

  /**
   * Gets the template as a string, defined in 'template: html'
   * or as file content, definend in 'template: file'.
   * @param {Object} view
   * @param {Function} callback
   */
  $scope.getTemplate = function(view, callback) {
    var file = view.template.file;

    if(angular.isDefined(file)) {
      $http.get(appRoot + file).success(function(template) {
        callback(template);
      });
    } else {
      callback(view.template.html);
    }
  };

  $scope.showFirstTab = function() {
    $scope.loading = false;
    var menu = angular.element('#tabmenu a:first');
    if(menu.length) {
      menu.tab('show');
    }
    if(angular.isDefined($scope.openView) && angular.isDefined($scope.viewsData[0])) {
      $scope.openView($scope.viewsData[0].id);
    }
  };

  /**
   * Changes the startURI, the whole workflow is starting new.
   * @param  {String} uri new startURI
   */
  $scope.changeURI = function(uri) {
    $location.search({uri: uri}).replace();
    $scope.count = 0;
    $scope.prevResource = $scope.ld;
    $scope.startURI = uri;
    $scope.init();
  };

  /**
   * Changes the main resource and also startURI and
   * view automatically.
   * @param  {Object} resource
   */
  $scope.changeResource = function(resource) {
    $location.search({uri: resource['@id']}).replace();
    delete resource['@context'];
    $scope.count = 0;
    $scope.prevResource = $scope.ld;
    $scope.ld = resource;
    var input = angular.element('#startURI');
    if(input.length) {
      input.val(resource['@id']);
    }
    var iframe = angular.element('#wrapper iframe');
    if(iframe.length) {
      iframe.attr('src', resource['@id']);
    }
  };

  $scope.$watch(function () { return ($location.search()).uri; }, function (uri) {
    if (uri) {
      $scope.startURI = uri;
    }
});

  /**
   * If input is an array return the joined elements,
   * if not return the unchanged input.
   * @param  {Array|String} input can be array or string
   * @param  {String} separator, default is ', '
   * @return {String}
   */
  $scope.join = function(input, separator) {
    separator = angular.isUndefined(separator) ? ', ' : separator;
    return angular.isArray(input) ? input.join(separator) : input;
  };

  /**
   * If input is an array with objects return the joined names,
   * if not return only the name of the object.
   * @param  {Array|Object} input can be array or object
   * @return {String}
   */
  $scope.uniqueNames = function(input) {
    if(angular.isUndefined(input)) { return; }

    if(angular.isArray(input)) {
      var names = [];
      angular.forEach(input, function(obj) {
        if(angular.isDefined(obj.name)) {
          names.push(obj.name);
        }
      });
      return $scope.join(names);
    }
    return input.name;
  };

  /**
   * URL validation
   * @param  {String}  value url string
   * @return {Boolean}
   */
  $scope.isURL = function(value) {
    $scope.warning = false;
    var regexp = /(^|\s)((https?:\/\/)?[\w\-]+(\.[\w\-]+)+\.?(:\d+)?(\/\S*)?)/gi;

    if(!regexp.test(value)) { // not a valid uri
      $scope.urierror = true;
      return false;
    }

    $scope.urierror = false;
    return true;
  };

});

/**
 * Manual Initialization of angular app
 */
angular.element(document).ready(function() {
  angular.bootstrap(document, ['visuaLOD']);
});

