/**
 * only for bookmarklet
 */
visuaLOD.run(function ($rootScope) {
  var derivedBy = angular.element('head link[rev=describedby]')[0],
      alternateLink = derivedBy || angular.element('head link[rel=alternate]')[0];

  $rootScope.startURI = angular.isDefined(alternateLink) ? alternateLink.href : location.href;
  $rootScope.appRoot = 'http://bettysteger.com/linkedmovie/';

  $rootScope.viewsPath = $rootScope.appRoot + "views/";
  $rootScope.views = ["actor", "person", "movie", "movie_lmdb", "person_lmdb", "place", "default"];

  $('#wrapper .view').hide();
  $rootScope.selectedView = undefined;

  $rootScope.openView = function(view) {
    $('header #views a').removeClass('active');
    $('#wrapper .view').hide();
    $('#wrapper iframe').hide();
    $('#wrapper #'+view).show();
    $rootScope.selectedView = view;
  };

  $rootScope.showIframe = function() {
    $rootScope.selectedView = undefined;
    $('header #views a').removeClass('active');
    $('#wrapper .view').hide();
    $('#wrapper iframe').show();
    $('header #views a').last().addClass('active');
  };
});
