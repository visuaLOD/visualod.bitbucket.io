var visuaLOD = angular.module('visuaLOD', []);

visuaLOD.service('ld', function($http, $q, $rootScope) {

  var self = this;

  this.getResource = function(uri, context) {
    if(!angular.isString(uri)) {
      var deferred = $q.defer();
      deferred.reject();
      return deferred.promise;
    }
    return self.getJSONLD(uri).then(function(jsonld) {
      return self.compactJSONLD(jsonld, context);
    });
  };

  /**
   * Converts Resource to JSON-LD via rdf-translator and
   * expands it because rdf-translator defines own context
   * @see https://rdf-translator.appspot.com
   * @param  {String} uri
   * @return {Promise}
   */
  this.getJSONLD = function(uri) {
    var deferred = $q.defer(),
        proxy = angular.isDefined($rootScope.proxy) ? $rootScope.proxy : '',
        uriToGet = encodeURIComponent(proxy + uri),
        error = 'rdf-translator throws an error with uri: ' + uriToGet;

    $http.get('https://rdf-translator2.appspot.com/convert/detect/json-ld/'+uriToGet, {cache:true}).success(function(data) {
      var graph = data['@graph'],
          resource = undefined;

      // rdf-translator returns resource with context, so expand it first, to match the real resource URI
      jsonld.expand(data, function(err, expanded) {

        angular.forEach(expanded, function(r) {
          if(r['@id'] === uri) { // matches uri
            resource = r;
          }
        });
        var jsonldData = resource || graph || data;

        if(!jsonldData) {
          deferred.reject(error);
        } else {
          $rootScope.$apply(deferred.resolve(jsonldData));
        }
      });

    }).error(function() {
      deferred.reject(error);
    });

    return deferred.promise;
  };

  /**
   * Uses the the JSON-LD Processor and API Implemenation of jsonld.js
   * to compact JSON-LD with the given context
   * @see https://github.com/digitalbazaar/jsonld.js
   * @param  {Object} input   JSON-LD input
   * @param  {Object} context JSON-LD context
   * @return {Promise}
   */
  this.compactJSONLD = function(input, context) {
    var deferred = $q.defer();

    jsonld.compact(input, context, function(err, compacted) {
      if(!err) {

        /**
         * removes @id so there is no need to write @type: @id in the json-ld context
         */
        angular.forEach(compacted, function(value, key){
          if(angular.isArray(value)) {
            angular.forEach(value, function(v, k){
              compacted[key][k] = v['@id'] ? v['@id'] : v;
            });
          } else {
            compacted[key] = value['@id'] ? value['@id'] : value;
            compacted[key] = (value['@type'] && (value['@type'] === 'http://www.w3.org/2001/XMLSchema#string' || value['@type'] === 'http://www.w3.org/1999/02/22-rdf-syntax-ns#langString')) ? value['@value'] : value;
          }
        });

        $rootScope.$apply(deferred.resolve(compacted));
      } else {
        deferred.reject('JSON-LD compacting');
      }
    });

    return deferred.promise;
  };
});
