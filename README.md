# visuaLOD - a Linked Data View Builder

visuaLOD is a fully client side library that allows to separate the workflow of the creation of Linked Data applications in a semantic web part and a UI part. It turnes complex graph structures to configurable object models and uses well known technologies like JavaScript and JSON. It was built via Google's AngularJS Framework. 

It is possible to create a view with linked data only by configuration.

## Demo

This project is work in progress, you can find the last released demo [here](http://visualod.bitbucket.io). At this moment the code itself is the running demo.

![DEMO](https://bitbucket.org/visuaLOD/visualod.bitbucket.io/raw/master/img/demo.gif)

## Installation

Include the Javascript file `bin/visualod.js`. 

If done, you can define a Linked Data Resource as a starting point, and as well the config files, where you define constraints and template (see View). 

```
visuaLOD.run(function ($rootScope) {
  $rootScope.startURI = "dbpedia.org/resource/The_Shawshank_Redemption";

  // uses views/actor.json, views/person.json ...
  $rootScope.viewsPath = "views/"; // folder or url (e.g. http://visualod.bitbucket.io/views/)
  $rootScope.views = ["actor", "person", "movie", "default"];
});
```

Always prioritize the views array in the order you want it to display. It is possible to call *visuaLOD.run* more than once to change the startURI dynamically.

This library uses [AngularJS](http://angularjs.org), so define a controller named "LDController" in the body or div tag where you want to show/render the templates. [ng-repeat](http://docs.angularjs.org/api/) is a loop in AngularJS. So all applicable templates will be renderend inside a div with the view's id. 

```
<body ng-controller="LDController">
  <div ng-repeat="view in viewsData" id="{{view.id}}"></div>
</body>
```

You can find an example how to use AngularJS in the **[index.html](https://bitbucket.org/visuaLOD/visualod.bitbucket.io/src/2e6636c63a0050bcbc64f2e0984177368a286162/index.html)** of the demo, there are several functions used: e.g. *ng-show*, *ng-submit*, *ng-click*. It is possible to bind an input field to the startURI (see index.html).

You can use the default view to see what data you get by requesting a resource.



## View (configuration file)

A view consists of 'id', 'constraints', 'context', 'fetch' and 'template'.


You have to understand how you can use a [JSON-LD](http://json-ld.org/) Context. Because we need it to define data that we can use in the template. 

#### JSON-LD Context
This **[JSON-LD example](http://json-ld.org/playground/index.html?json-ld=%7B%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fname%22%3A%22Manu%20Sporny%22%2C%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fnick%22%3A%7B%22%40value%22%3A%22Man%22%2C%22%40language%22%3A%22en%22%7D%2C%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fhomepage%22%3A%7B%22%40id%22%3A%22http%3A%2F%2Fmanu.sporny.org%2F%22%7D%7D&frame=%7B%7D&context=%7B%0A%20%20%22%40context%22%3A%20%7B%0A%20%20%20%20%22name%22%3A%20%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fname%22%2C%0A%20%20%20%20%22nick%22%3A%20%7B%0A%20%20%20%20%20%20%22%40id%22%3A%20%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fnick%22%2C%0A%20%20%20%20%20%20%22%40language%22%3A%20%22en%22%0A%20%20%20%20%7D%2C%0A%20%20%20%20%22homepage%22%3A%20%7B%0A%20%20%20%20%20%20%22%40id%22%3A%20%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fhomepage%22%2C%0A%20%20%20%20%20%20%22%40type%22%3A%20%22%40id%22%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D&startTab=tab-compacted)** describes a person. The JSON property 'name' is mapped to a concept in the [FOAF](http://xmlns.com/foaf/spec/) vocabulary. The value of 'nick' has a language tag, so you need to define that tag also inside the context. The value of the homepage property is specified to be of the type @id, i.e., it is specified to be an URI in the context definition. You should see the result in the Compacted Tab below.

Basically you work with different URIs to use the data you want to display.  
***HINT:*** Every `@id` is an URI. To get an URI you need to define an `"@type": "@id"` in the context. 


#### Structure

> **id**  
> type: string  
> identifier of this view
>
> **constraints** (optional)  
> can contain "type", "required" and "uri" (all optional),  
> if uri is defined it has to match with the given uri.  
> if one of the type matches AND all required attributes are available  
> the process starts and the template will be rendered.
>
> **context**  
> defines the data or rather the variables that can be used in the template,  
> see simple examples at [http://json-ld.org/playground](http://json-ld.org/playground/index.html?json-ld=%7B%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fname%22%3A%22Manu%20Sporny%22%2C%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fnick%22%3A%7B%22%40value%22%3A%22Man%22%2C%22%40language%22%3A%22en%22%7D%2C%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fhomepage%22%3A%7B%22%40id%22%3A%22http%3A%2F%2Fmanu.sporny.org%2F%22%7D%7D&frame=%7B%7D&context=%7B%0A%20%20%22%40context%22%3A%20%7B%0A%20%20%20%20%22name%22%3A%20%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fname%22%2C%0A%20%20%20%20%22nick%22%3A%20%7B%0A%20%20%20%20%20%20%22%40id%22%3A%20%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fnick%22%2C%0A%20%20%20%20%20%20%22%40language%22%3A%20%22en%22%0A%20%20%20%20%7D%2C%0A%20%20%20%20%22homepage%22%3A%20%7B%0A%20%20%20%20%20%20%22%40id%22%3A%20%22http%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fhomepage%22%2C%0A%20%20%20%20%20%20%22%40type%22%3A%20%22%40id%22%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D&startTab=tab-compacted)
>
> **fetch** (optional)  
> defines what data should be retrieved additionally,  
> this data has to be defined in the context and should contain one or more URIs
>
> **template**  
> "file" or "html" have to be defined, if both available it uses the file  
> - file: path should lead to a template file  
> - html: inline html template

#### Example
```
{
  "id": "person",
  "constraints": {
    "type": "http://xmlns.com/foaf/0.1/Person",
    "required": "name"
  },
  "context": {
    "name": {
      "@id": "http://xmlns.com/foaf/0.1/name",
      "@language": "en"
    },
    "nick": {
      "@id": "http://xmlns.com/foaf/0.1/nick",
      "@language": "en"
    },
    "birthPlace": {
      "@id": "http://dbpedia.org/ontology/birthPlace",
      "@type": "@id",
      "@container": "@set"
    }
  },
  "fetch": ["birthPlace"],
  "template": {
    "html": "<h2>{{name}} <small>({{nick}})</small></h2><p>is a person, born in: {{actor.birthPlace.0.name}}</p>"
  }
}
```

## Template

All variables defined in the context can be used (if this data is available) in the template. See the following example:

* context in view.json: `"name": "http://xmlns.com/foaf/0.1/name"`

* usage in template.html: `{{name}}`


#### Functions

Any AngularJS function can be used inside the template, e.g. `ng-show`, `ng-repeat` ([Documentation](http://docs.angularjs.org/api/)). The following functions are defined in visuaLOD, see examples in 'views/movie.html' or 'views/default.html'.

* `changeURI(uri)`  
  Changes the startURI, the whole workflow is starting new. Needs to be an URI.
* `changeResource(resource)`  
  Changes the main resource and also startURI and view automatically. Needs to be an object.
* `join(input, separator)`  
  If input is an array return the joined elements, if not return the unchanged input. Default separator is ', '.
* `uniqueNames(input)`  
  If input is an array with objects return the joined names, if not return only the name of the object.


#### Nested Templates

An @ before a term defines a nested view/template. It shows its template if constraints are true, if false it ignores it!
```
{{@view_id}}

<div class='span3'>{{@thumbnail}}</div> <div class='span6'><h2>{{name}}</h2></div>
```


## Usage

Use any resource (eg. http://dbpedia.org/resource/Fight_Club) as Start URI. The following views are already defined in '[/views](https://bitbucket.org/visuaLOD/visualod.bitbucket.org/src/2e6636c63a0050bcbc64f2e0984177368a286162/views)' for the demo:

* default
* movie
* actor
* person
* place
* thumbnail

For example you want to display a place, e.g. Paris. You'll need to define `http://sws.geonames.org/2988507/` as startURI and add a new view (config file), e.g. `place.json`, where you define constraints and the JSON-LD context. Add the filename without extension: `$rootScope.views = ['place'];`. 


Published Paper about VisuaLOD: https://www.dropbox.com/s/67xed4m4ps6v1qf/visuaLOD.pdf?dl=0



(c) 2013 - Bettina Steger, BSc